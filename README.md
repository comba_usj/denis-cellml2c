DENIS-CellML2C
==============

Tool to export CellML files to C++ to be included in DENIS Myocyte Simulator


## Iinstallation

### Linux
+ Install and/or update the dependencies:

```bash
    sudo apt update
    sudo apt upgrade
    sudo apt install -y git m4 make g++ cmake omniidl omniorb graphviz
```

+ Create the directory structure to store all the programs that you need:

```bash
    mkdir ~/DENIS_repositories 
    cd ~/DENIS_repositories
```

+ Get the CellML API and CellML2C reposotories:

```bash
    git clone https://bitbucket.org/usj_bsicos/cellml-api
    git clone https://bitbucket.org/usj_bsicos/denis-cellml2c
```

+ Compile the CellML API:
```bash
    cd cellml-api
    cmake -LA -DENABLE_CELEDS_EXPORTER=ON -DENABLE_ANNOTOOLS=ON -DNABLE_CUSES=ON -DENABLE_VACSS=ON .
    make
    sudo make install
    cd ..
    echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib' >> ~/.bashrc
    bash
```

+ Compile the program:

```bash
    cd denis-cellml2c
    mkdir cmake-build
	cd cmake-build
    cmake ..
    make
    cd ../..
```

+ Create an alias to run the program:
```bash
    echo "alias denis_cellml2c='~/DENIS_repositories/denis-cellml2c/cmake-build/denis_cellml2c'" >> ~/.bash_aliases
    bash
```