/*======================================================================
                                                                        
                      --- DENIS Project ---                             
                        -----------------                               
                                                                        
      Distributed computing                                             
      Electrophysiologycal Models                                       
      Networking collaboration                                          
      In Silico research                                                
      Sharing Knowledge                                                 
                                                                        
------------------------------------------------------------------------
                                                                        
 -- C Models Library created for the DENIS Project --                   
                                                                        
 This file has been automatically created using the CellML API.         
                                                                        
------------------------------------------------------------------------
                                                                        
 DENIS-CellML2C Copyright 2020 J. Carro (jcarro@usj.es)                 
                                                                        
 Licensed under the Apache License, Version 2.0 (the "License");      
 you may not use this file except in compliance with the License.       
 You may obtain a copy of the License at                                
                                                                        
 http://www.apache.org/licenses/LICENSE-2.0                             
                                                                        
 Unless required by applicable law or agreed to in writing, software    
 distributed under the License is distributed on an "AS IS" BASIS,    
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or        
 implied. See the License for the specific language governing           
 permissions and limitations under the License.                         
                                                                        
------------------------------------------------------------------------
                                                                        
                                                 Universidad San Jorge  
                                 School of Architecture and Technology  
                                                                        
                                                  https://denis.usj.es  
                                                                        
======================================================================*/

#include "models.h"

Model* getModelByName(const char* modelName){
	if(strcmp(modelName, "CNM_opt_ENDO_SS_biphasic")==0)
		return new CNM_opt_ENDO_SS_biphasic();
	if(strcmp(modelName, "TP06_ENDO_SS_biphasic")==0)
		return new TP06_ENDO_SS_biphasic();
	if(strcmp(modelName, "CRLP_ENDO_SS_biphasic")==0)
		return new CRLP_ENDO_SS_biphasic();
	if(strcmp(modelName, "GPB_ENDO_SS_biphasic")==0)
		return new GPB_ENDO_SS_biphasic();
	if(strcmp(modelName, "ORd_ENDO_SS_biphasic")==0)
		return new ORd_ENDO_SS_biphasic();
	return NULL;
}
