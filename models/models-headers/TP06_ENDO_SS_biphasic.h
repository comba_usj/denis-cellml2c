/*======================================================================
                                                                        
                      --- DENIS Project ---                             
                        -----------------                               
                                                                        
      Distributed computing                                             
      Electrophysiologycal Models                                       
      Networking colaboration                                           
      In Silico research                                                
      Sharing Knowledge                                                 
                                                                        
------------------------------------------------------------------------
                                                                        
 -- C File of the TP06_ENDO_SS_biphasic model created for the DENIS Project -- 
                                                                        
 This file has been automatically created using the CellML API.         
                                                                        
------------------------------------------------------------------------
                                                                        
 DENIS-CellML2C Copyright 2020, J. Carro (jcarro@usj.es)                
                                                                        
 Licensed under the Apache License, Version 2.0 (the "License");      
 you may not use this file except in compliance with the License.       
 You may obtain a copy of the License at                                
                                                                        
 http://www.apache.org/licenses/LICENSE-2.0                             
                                                                        
 Unless required by applicable law or agreed to in writing, software    
 distributed under the License is distributed on an "AS IS" BASIS,    
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or        
 implied. See the License for the specific language governing           
 permissions and limitations under the License.                         
                                                                        
------------------------------------------------------------------------
                                                                        
                                                 Universidad San Jorge  
                                 School of Architecture and Technology  
                                                                        
                                                  https://denis.usj.es  
                                                                        
======================================================================*/

#include "Model.h"

#pragma once

using namespace std;

class TP06_ENDO_SS_biphasic:public Model{
public:
	TP06_ENDO_SS_biphasic(){

        setVectorsLength();
        initVectors();
    }

    void setVectorsLength(){
        algebraicsLength = 74;
        statesLength = 19;
        constantsLength = 84;
	}
		
	void setNames(){
        const char** constants = constantsNames;
        const char** rates = ratesNames;
        const char** states = statesNames;
        const char** algebraic = algebraicNames;

 		VoI = "time in component environment (millisecond)";
 		states[0] = "V in component membrane (millivolt)";
 		constants[0] = "R in component membrane (joule_per_mole_kelvin)";
 		constants[1] = "T in component membrane (kelvin)";
 		constants[2] = "F in component membrane (coulomb_per_millimole)";
 		constants[3] = "Cm in component membrane (microF)";
 		constants[4] = "V_c in component membrane (micrometre3)";
 		algebraic[34] = "t_Cycle in component membrane (millisecond)";
 		constants[73] = "t_0 in component membrane (millisecond)";
 		constants[74] = "t_1 in component membrane (millisecond)";
 		constants[75] = "t_2 in component membrane (millisecond)";
 		constants[76] = "t_3 in component membrane (millisecond)";
 		constants[77] = "t_4 in component membrane (millisecond)";
 		constants[78] = "t_5 in component membrane (millisecond)";
 		constants[79] = "t_6 in component membrane (millisecond)";
 		constants[80] = "t_7 in component membrane (millisecond)";
 		constants[81] = "t_8 in component membrane (millisecond)";
 		constants[82] = "t_9 in component membrane (millisecond)";
 		constants[83] = "t_10 in component membrane (millisecond)";
 		constants[5] = "nCL_0 in component membrane (dimensionless)";
 		constants[6] = "nCL_1 in component membrane (dimensionless)";
 		constants[7] = "nCL_2 in component membrane (dimensionless)";
 		constants[8] = "nCL_3 in component membrane (dimensionless)";
 		constants[9] = "nCL_4 in component membrane (dimensionless)";
 		constants[10] = "nCL_5 in component membrane (dimensionless)";
 		constants[11] = "nCL_6 in component membrane (dimensionless)";
 		constants[12] = "nCL_7 in component membrane (dimensionless)";
 		constants[13] = "nCL_8 in component membrane (dimensionless)";
 		constants[14] = "nCL_9 in component membrane (dimensionless)";
 		constants[15] = "nCL_10 in component membrane (dimensionless)";
 		constants[16] = "CL_0 in component membrane (millisecond)";
 		constants[17] = "CL_1 in component membrane (millisecond)";
 		constants[18] = "CL_2 in component membrane (millisecond)";
 		constants[19] = "CL_3 in component membrane (millisecond)";
 		constants[20] = "CL_4 in component membrane (millisecond)";
 		constants[21] = "CL_5 in component membrane (millisecond)";
 		constants[22] = "CL_6 in component membrane (millisecond)";
 		constants[23] = "CL_7 in component membrane (millisecond)";
 		constants[24] = "CL_8 in component membrane (millisecond)";
 		constants[25] = "CL_9 in component membrane (millisecond)";
 		constants[26] = "CL_10 in component membrane (millisecond)";
 		algebraic[51] = "i_K1 in component inward_rectifier_potassium_current (picoA_per_picoF)";
 		algebraic[58] = "i_to in component transient_outward_current (picoA_per_picoF)";
 		algebraic[52] = "i_Kr in component rapid_time_dependent_potassium_current (picoA_per_picoF)";
 		algebraic[53] = "i_Ks in component slow_time_dependent_potassium_current (picoA_per_picoF)";
 		algebraic[56] = "i_CaL in component L_type_Ca_current (picoA_per_picoF)";
 		algebraic[59] = "i_NaK in component sodium_potassium_pump_current (picoA_per_picoF)";
 		algebraic[54] = "i_Na in component fast_sodium_current (picoA_per_picoF)";
 		algebraic[55] = "i_b_Na in component sodium_background_current (picoA_per_picoF)";
 		algebraic[60] = "i_NaCa in component sodium_calcium_exchanger_current (picoA_per_picoF)";
 		algebraic[57] = "i_b_Ca in component calcium_background_current (picoA_per_picoF)";
 		algebraic[62] = "i_p_K in component potassium_pump_current (picoA_per_picoF)";
 		algebraic[61] = "i_p_Ca in component calcium_pump_current (picoA_per_picoF)";
 		algebraic[42] = "i_Stim in component membrane (picoA_per_picoF)";
 		algebraic[13] = "I_Stim_Start in component membrane (millisecond)";
 		algebraic[0] = "I_Stim_End in component membrane (millisecond)";
 		constants[27] = "I_Stim_Amplitude in component membrane (picoA_per_picoF)";
 		constants[28] = "I_Stim_PulseDuration in component membrane (millisecond)";
 		algebraic[26] = "I_Stim_CL in component membrane (millisecond)";
 		algebraic[44] = "E_Na in component reversal_potentials (millivolt)";
 		algebraic[45] = "E_K in component reversal_potentials (millivolt)";
 		algebraic[46] = "E_Ks in component reversal_potentials (millivolt)";
 		algebraic[47] = "E_Ca in component reversal_potentials (millivolt)";
 		constants[29] = "P_kna in component reversal_potentials (dimensionless)";
 		constants[30] = "K_o in component potassium_dynamics (millimolar)";
 		constants[31] = "Na_o in component sodium_dynamics (millimolar)";
 		states[1] = "K_i in component potassium_dynamics (millimolar)";
 		states[2] = "Na_i in component sodium_dynamics (millimolar)";
 		constants[32] = "Ca_o in component calcium_dynamics (millimolar)";
 		states[3] = "Ca_i in component calcium_dynamics (millimolar)";
 		constants[33] = "G_K1 in component inward_rectifier_potassium_current (nanoS_per_picoF)";
 		algebraic[50] = "xK1_inf in component inward_rectifier_potassium_current (dimensionless)";
 		algebraic[48] = "alpha_K1 in component inward_rectifier_potassium_current (dimensionless)";
 		algebraic[49] = "beta_K1 in component inward_rectifier_potassium_current (dimensionless)";
 		constants[34] = "G_Kr in component rapid_time_dependent_potassium_current (nanoS_per_picoF)";
 		states[4] = "Xr1 in component rapid_time_dependent_potassium_current_Xr1_gate (dimensionless)";
 		states[5] = "Xr2 in component rapid_time_dependent_potassium_current_Xr2_gate (dimensionless)";
 		algebraic[1] = "xr1_inf in component rapid_time_dependent_potassium_current_Xr1_gate (dimensionless)";
 		algebraic[14] = "alpha_xr1 in component rapid_time_dependent_potassium_current_Xr1_gate (dimensionless)";
 		algebraic[27] = "beta_xr1 in component rapid_time_dependent_potassium_current_Xr1_gate (dimensionless)";
 		algebraic[35] = "tau_xr1 in component rapid_time_dependent_potassium_current_Xr1_gate (millisecond)";
 		algebraic[2] = "xr2_inf in component rapid_time_dependent_potassium_current_Xr2_gate (dimensionless)";
 		algebraic[15] = "alpha_xr2 in component rapid_time_dependent_potassium_current_Xr2_gate (dimensionless)";
 		algebraic[28] = "beta_xr2 in component rapid_time_dependent_potassium_current_Xr2_gate (dimensionless)";
 		algebraic[36] = "tau_xr2 in component rapid_time_dependent_potassium_current_Xr2_gate (millisecond)";
 		constants[35] = "G_Ks in component slow_time_dependent_potassium_current (nanoS_per_picoF)";
 		states[6] = "Xs in component slow_time_dependent_potassium_current_Xs_gate (dimensionless)";
 		algebraic[3] = "xs_inf in component slow_time_dependent_potassium_current_Xs_gate (dimensionless)";
 		algebraic[16] = "alpha_xs in component slow_time_dependent_potassium_current_Xs_gate (dimensionless)";
 		algebraic[29] = "beta_xs in component slow_time_dependent_potassium_current_Xs_gate (dimensionless)";
 		algebraic[37] = "tau_xs in component slow_time_dependent_potassium_current_Xs_gate (millisecond)";
 		constants[36] = "g_Na in component fast_sodium_current (nanoS_per_picoF)";
 		states[7] = "m in component fast_sodium_current_m_gate (dimensionless)";
 		states[8] = "h in component fast_sodium_current_h_gate (dimensionless)";
 		states[9] = "j in component fast_sodium_current_j_gate (dimensionless)";
 		algebraic[4] = "m_inf in component fast_sodium_current_m_gate (dimensionless)";
 		algebraic[17] = "alpha_m in component fast_sodium_current_m_gate (dimensionless)";
 		algebraic[30] = "beta_m in component fast_sodium_current_m_gate (dimensionless)";
 		algebraic[38] = "tau_m in component fast_sodium_current_m_gate (millisecond)";
 		algebraic[5] = "h_inf in component fast_sodium_current_h_gate (dimensionless)";
 		algebraic[18] = "alpha_h in component fast_sodium_current_h_gate (per_millisecond)";
 		algebraic[31] = "beta_h in component fast_sodium_current_h_gate (per_millisecond)";
 		algebraic[39] = "tau_h in component fast_sodium_current_h_gate (millisecond)";
 		algebraic[6] = "j_inf in component fast_sodium_current_j_gate (dimensionless)";
 		algebraic[19] = "alpha_j in component fast_sodium_current_j_gate (per_millisecond)";
 		algebraic[32] = "beta_j in component fast_sodium_current_j_gate (per_millisecond)";
 		algebraic[40] = "tau_j in component fast_sodium_current_j_gate (millisecond)";
 		constants[37] = "g_bna in component sodium_background_current (nanoS_per_picoF)";
 		constants[38] = "g_CaL in component L_type_Ca_current (litre_per_farad_second)";
 		states[10] = "Ca_ss in component calcium_dynamics (millimolar)";
 		states[11] = "d in component L_type_Ca_current_d_gate (dimensionless)";
 		states[12] = "f in component L_type_Ca_current_f_gate (dimensionless)";
 		states[13] = "f2 in component L_type_Ca_current_f2_gate (dimensionless)";
 		states[14] = "fCass in component L_type_Ca_current_fCass_gate (dimensionless)";
 		algebraic[7] = "d_inf in component L_type_Ca_current_d_gate (dimensionless)";
 		algebraic[20] = "alpha_d in component L_type_Ca_current_d_gate (dimensionless)";
 		algebraic[33] = "beta_d in component L_type_Ca_current_d_gate (dimensionless)";
 		algebraic[41] = "gamma_d in component L_type_Ca_current_d_gate (millisecond)";
 		algebraic[43] = "tau_d in component L_type_Ca_current_d_gate (millisecond)";
 		algebraic[8] = "f_inf in component L_type_Ca_current_f_gate (dimensionless)";
 		algebraic[21] = "tau_f in component L_type_Ca_current_f_gate (millisecond)";
 		algebraic[9] = "f2_inf in component L_type_Ca_current_f2_gate (dimensionless)";
 		algebraic[22] = "tau_f2 in component L_type_Ca_current_f2_gate (millisecond)";
 		algebraic[10] = "fCass_inf in component L_type_Ca_current_fCass_gate (dimensionless)";
 		algebraic[23] = "tau_fCass in component L_type_Ca_current_fCass_gate (millisecond)";
 		constants[39] = "g_bca in component calcium_background_current (nanoS_per_picoF)";
 		constants[40] = "G_to in component transient_outward_current (nanoS_per_picoF)";
 		states[15] = "s in component transient_outward_current_s_gate (dimensionless)";
 		states[16] = "r in component transient_outward_current_r_gate (dimensionless)";
 		algebraic[11] = "s_inf in component transient_outward_current_s_gate (dimensionless)";
 		algebraic[24] = "tau_s in component transient_outward_current_s_gate (millisecond)";
 		algebraic[12] = "r_inf in component transient_outward_current_r_gate (dimensionless)";
 		algebraic[25] = "tau_r in component transient_outward_current_r_gate (millisecond)";
 		constants[41] = "P_NaK in component sodium_potassium_pump_current (picoA_per_picoF)";
 		constants[42] = "K_mk in component sodium_potassium_pump_current (millimolar)";
 		constants[43] = "K_mNa in component sodium_potassium_pump_current (millimolar)";
 		constants[44] = "K_NaCa in component sodium_calcium_exchanger_current (picoA_per_picoF)";
 		constants[45] = "K_sat in component sodium_calcium_exchanger_current (dimensionless)";
 		constants[46] = "alpha in component sodium_calcium_exchanger_current (dimensionless)";
 		constants[47] = "gamma in component sodium_calcium_exchanger_current (dimensionless)";
 		constants[48] = "Km_Ca in component sodium_calcium_exchanger_current (millimolar)";
 		constants[49] = "Km_Nai in component sodium_calcium_exchanger_current (millimolar)";
 		constants[50] = "g_pCa in component calcium_pump_current (picoA_per_picoF)";
 		constants[51] = "K_pCa in component calcium_pump_current (millimolar)";
 		constants[52] = "g_pK in component potassium_pump_current (nanoS_per_picoF)";
 		states[17] = "Ca_SR in component calcium_dynamics (millimolar)";
 		algebraic[71] = "i_rel in component calcium_dynamics (millimolar_per_millisecond)";
 		algebraic[63] = "i_up in component calcium_dynamics (millimolar_per_millisecond)";
 		algebraic[64] = "i_leak in component calcium_dynamics (millimolar_per_millisecond)";
 		algebraic[65] = "i_xfer in component calcium_dynamics (millimolar_per_millisecond)";
 		algebraic[70] = "O in component calcium_dynamics (dimensionless)";
 		states[18] = "R_prime in component calcium_dynamics (dimensionless)";
 		algebraic[68] = "k1 in component calcium_dynamics (per_millimolar2_per_millisecond)";
 		algebraic[69] = "k2 in component calcium_dynamics (per_millimolar_per_millisecond)";
 		constants[53] = "k1_prime in component calcium_dynamics (per_millimolar2_per_millisecond)";
 		constants[54] = "k2_prime in component calcium_dynamics (per_millimolar_per_millisecond)";
 		constants[55] = "k3 in component calcium_dynamics (per_millisecond)";
 		constants[56] = "k4 in component calcium_dynamics (per_millisecond)";
 		constants[57] = "EC in component calcium_dynamics (millimolar)";
 		constants[58] = "max_sr in component calcium_dynamics (dimensionless)";
 		constants[59] = "min_sr in component calcium_dynamics (dimensionless)";
 		algebraic[66] = "kcasr in component calcium_dynamics (dimensionless)";
 		constants[60] = "V_rel in component calcium_dynamics (per_millisecond)";
 		constants[61] = "V_xfer in component calcium_dynamics (per_millisecond)";
 		constants[62] = "K_up in component calcium_dynamics (millimolar)";
 		constants[63] = "V_leak in component calcium_dynamics (per_millisecond)";
 		constants[64] = "Vmax_up in component calcium_dynamics (millimolar_per_millisecond)";
 		algebraic[67] = "Ca_i_bufc in component calcium_dynamics (dimensionless)";
 		algebraic[72] = "Ca_sr_bufsr in component calcium_dynamics (dimensionless)";
 		algebraic[73] = "Ca_ss_bufss in component calcium_dynamics (dimensionless)";
 		constants[65] = "Buf_c in component calcium_dynamics (millimolar)";
 		constants[66] = "K_buf_c in component calcium_dynamics (millimolar)";
 		constants[67] = "Buf_sr in component calcium_dynamics (millimolar)";
 		constants[68] = "K_buf_sr in component calcium_dynamics (millimolar)";
 		constants[69] = "Buf_ss in component calcium_dynamics (millimolar)";
 		constants[70] = "K_buf_ss in component calcium_dynamics (millimolar)";
 		constants[71] = "V_sr in component calcium_dynamics (micrometre3)";
 		constants[72] = "V_ss in component calcium_dynamics (micrometre3)";
 		rates[0] = "d/dt V in component membrane (millivolt)";
 		rates[4] = "d/dt Xr1 in component rapid_time_dependent_potassium_current_Xr1_gate (dimensionless)";
 		rates[5] = "d/dt Xr2 in component rapid_time_dependent_potassium_current_Xr2_gate (dimensionless)";
 		rates[6] = "d/dt Xs in component slow_time_dependent_potassium_current_Xs_gate (dimensionless)";
 		rates[7] = "d/dt m in component fast_sodium_current_m_gate (dimensionless)";
 		rates[8] = "d/dt h in component fast_sodium_current_h_gate (dimensionless)";
 		rates[9] = "d/dt j in component fast_sodium_current_j_gate (dimensionless)";
 		rates[11] = "d/dt d in component L_type_Ca_current_d_gate (dimensionless)";
 		rates[12] = "d/dt f in component L_type_Ca_current_f_gate (dimensionless)";
 		rates[13] = "d/dt f2 in component L_type_Ca_current_f2_gate (dimensionless)";
 		rates[14] = "d/dt fCass in component L_type_Ca_current_fCass_gate (dimensionless)";
 		rates[15] = "d/dt s in component transient_outward_current_s_gate (dimensionless)";
 		rates[16] = "d/dt r in component transient_outward_current_r_gate (dimensionless)";
 		rates[18] = "d/dt R_prime in component calcium_dynamics (dimensionless)";
 		rates[3] = "d/dt Ca_i in component calcium_dynamics (millimolar)";
 		rates[17] = "d/dt Ca_SR in component calcium_dynamics (millimolar)";
 		rates[10] = "d/dt Ca_ss in component calcium_dynamics (millimolar)";
 		rates[2] = "d/dt Na_i in component sodium_dynamics (millimolar)";
 		rates[1] = "d/dt K_i in component potassium_dynamics (millimolar)";
	}

    void initConstsAndStates(){
    double* states  = initStates;

		states[0] = -85.23;
		constants[0] = 8314.472;
		constants[1] = 310;
		constants[2] = 96485.3415;
		constants[3] = 0.185;
		constants[4] = 0.016404;
		constants[5] = 3000;
		constants[6] = 25;
		constants[7] = 25;
		constants[8] = 25;
		constants[9] = 25;
		constants[10] = 25;
		constants[11] = 25;
		constants[12] = 25;
		constants[13] = 25;
		constants[14] = 25;
		constants[15] = 25;
		constants[16] = 1000;
		constants[17] = 300;
		constants[18] = 400;
		constants[19] = 500;
		constants[20] = 700;
		constants[21] = 1000;
		constants[22] = 1500;
		constants[23] = 2000;
		constants[24] = 3000;
		constants[25] = 5000;
		constants[26] = 10000;
		constants[27] = -52;
		constants[28] = 1;
		constants[29] = 0.03;
		constants[30] = 5.4;
		constants[31] = 140;
		states[1] = 138.4;
		states[2] = 10.355;
		constants[32] = 2;
		states[3] = 0.00013;
		constants[33] = 5.405;
		constants[34] = 0.153;
		states[4] = 0.00448;
		states[5] = 0.476;
		constants[35] = 0.392;
		states[6] = 0.0087;
		constants[36] = 14.838;
		states[7] = 0.00155;
		states[8] = 0.7573;
		states[9] = 0.7225;
		constants[37] = 0.00029;
		constants[38] = 0.0000398;
		states[10] = 0.00036;
		states[11] = 3.164e-5;
		states[12] = 0.8009;
		states[13] = 0.9778;
		states[14] = 0.9953;
		constants[39] = 0.000592;
		constants[40] = 0.073;
		states[15] = 0.3212;
		states[16] = 2.235e-8;
		constants[41] = 2.724;
		constants[42] = 1;
		constants[43] = 40;
		constants[44] = 1000;
		constants[45] = 0.1;
		constants[46] = 2.5;
		constants[47] = 0.35;
		constants[48] = 1.38;
		constants[49] = 87.5;
		constants[50] = 0.1238;
		constants[51] = 0.0005;
		constants[52] = 0.0146;
		states[17] = 3.715;
		states[18] = 0.9068;
		constants[53] = 0.15;
		constants[54] = 0.045;
		constants[55] = 0.06;
		constants[56] = 0.005;
		constants[57] = 1.5;
		constants[58] = 2.5;
		constants[59] = 1;
		constants[60] = 0.102;
		constants[61] = 0.0038;
		constants[62] = 0.00025;
		constants[63] = 0.00036;
		constants[64] = 0.006375;
		constants[65] = 0.2;
		constants[66] = 0.001;
		constants[67] = 10;
		constants[68] = 0.3;
		constants[69] = 0.4;
		constants[70] = 0.00025;
		constants[71] = 0.001094;
		constants[72] = 0.00005468;
	}

	void setDependentConsts(){
		constants[73] =  constants[5]*constants[16];
		constants[74] =  constants[6]*constants[17]+constants[73];
		constants[75] =  constants[7]*constants[18]+constants[74];
		constants[76] =  constants[8]*constants[19]+constants[75];
		constants[77] =  constants[9]*constants[20]+constants[76];
		constants[78] =  constants[10]*constants[21]+constants[77];
		constants[79] =  constants[11]*constants[22]+constants[78];
		constants[80] =  constants[12]*constants[23]+constants[79];
		constants[81] =  constants[13]*constants[24]+constants[80];
		constants[82] =  constants[14]*constants[25]+constants[81];
		constants[83] =  constants[15]*constants[26]+constants[82];
  }

	void computeRates(double VoI, double* rates, double* states, double* algebraic){

		algebraic[8] = 1.00000/(1.00000+exp((states[0]+20.0000)/7.00000));
		algebraic[21] =  1102.50*exp(- pow(states[0]+27.0000, 2.00000)/225.000)+200.000/(1.00000+exp((13.0000 - states[0])/10.0000))+180.000/(1.00000+exp((states[0]+30.0000)/10.0000))+20.0000;
		rates[12] = (algebraic[8] - states[12])/algebraic[21];
		algebraic[9] = 0.670000/(1.00000+exp((states[0]+35.0000)/7.00000))+0.330000;
		algebraic[22] =  562.000*exp(- pow(states[0]+27.0000, 2.00000)/240.000)+31.0000/(1.00000+exp((25.0000 - states[0])/10.0000))+80.0000/(1.00000+exp((states[0]+30.0000)/10.0000));
		rates[13] = (algebraic[9] - states[13])/algebraic[22];
		algebraic[10] = 0.600000/(1.00000+pow(states[10]/0.0500000, 2.00000))+0.400000;
		algebraic[23] = 80.0000/(1.00000+pow(states[10]/0.0500000, 2.00000))+2.00000;
		rates[14] = (algebraic[10] - states[14])/algebraic[23];
		algebraic[11] = 1.00000/(1.00000+exp((states[0]+28.0000)/5.00000));
		algebraic[24] =  1000.00*exp(- pow(states[0]+67.0000, 2.00000)/1000.00)+8.00000;
		rates[15] = (algebraic[11] - states[15])/algebraic[24];
		algebraic[12] = 1.00000/(1.00000+exp((20.0000 - states[0])/6.00000));
		algebraic[25] =  9.50000*exp(- pow(states[0]+40.0000, 2.00000)/1800.00)+0.800000;
		rates[16] = (algebraic[12] - states[16])/algebraic[25];
		algebraic[1] = 1.00000/(1.00000+exp((- 26.0000 - states[0])/7.00000));
		algebraic[14] = 450.000/(1.00000+exp((- 45.0000 - states[0])/10.0000));
		algebraic[27] = 6.00000/(1.00000+exp((states[0]+30.0000)/11.5000));
		algebraic[35] =  1.00000*algebraic[14]*algebraic[27];
		rates[4] = (algebraic[1] - states[4])/algebraic[35];
		algebraic[2] = 1.00000/(1.00000+exp((states[0]+88.0000)/24.0000));
		algebraic[15] = 3.00000/(1.00000+exp((- 60.0000 - states[0])/20.0000));
		algebraic[28] = 1.12000/(1.00000+exp((states[0] - 60.0000)/20.0000));
		algebraic[36] =  1.00000*algebraic[15]*algebraic[28];
		rates[5] = (algebraic[2] - states[5])/algebraic[36];
		algebraic[3] = 1.00000/(1.00000+exp((- 5.00000 - states[0])/14.0000));
		algebraic[16] = 1400.00/ pow((1.00000+exp((5.00000 - states[0])/6.00000)), 1.0 / 2);
		algebraic[29] = 1.00000/(1.00000+exp((states[0] - 35.0000)/15.0000));
		algebraic[37] =  1.00000*algebraic[16]*algebraic[29]+80.0000;
		rates[6] = (algebraic[3] - states[6])/algebraic[37];
		algebraic[4] = 1.00000/pow(1.00000+exp((- 56.8600 - states[0])/9.03000), 2.00000);
		algebraic[17] = 1.00000/(1.00000+exp((- 60.0000 - states[0])/5.00000));
		algebraic[30] = 0.100000/(1.00000+exp((states[0]+35.0000)/5.00000))+0.100000/(1.00000+exp((states[0] - 50.0000)/200.000));
		algebraic[38] =  1.00000*algebraic[17]*algebraic[30];
		rates[7] = (algebraic[4] - states[7])/algebraic[38];
		algebraic[5] = 1.00000/pow(1.00000+exp((states[0]+71.5500)/7.43000), 2.00000);
		algebraic[18] = (states[0]<- 40.0000 ?  0.0570000*exp(- (states[0]+80.0000)/6.80000) : 0.00000);
		algebraic[31] = (states[0]<- 40.0000 ?  2.70000*exp( 0.0790000*states[0])+ 310000.*exp( 0.348500*states[0]) : 0.770000/( 0.130000*(1.00000+exp((states[0]+10.6600)/- 11.1000))));
		algebraic[39] = 1.00000/(algebraic[18]+algebraic[31]);
		rates[8] = (algebraic[5] - states[8])/algebraic[39];
		algebraic[6] = 1.00000/pow(1.00000+exp((states[0]+71.5500)/7.43000), 2.00000);
		algebraic[19] = (states[0]<- 40.0000 ? (( ( - 25428.0*exp( 0.244400*states[0]) -  6.94800e-06*exp( - 0.0439100*states[0]))*(states[0]+37.7800))/1.00000)/(1.00000+exp( 0.311000*(states[0]+79.2300))) : 0.00000);
		algebraic[32] = (states[0]<- 40.0000 ? ( 0.0242400*exp( - 0.0105200*states[0]))/(1.00000+exp( - 0.137800*(states[0]+40.1400))) : ( 0.600000*exp( 0.0570000*states[0]))/(1.00000+exp( - 0.100000*(states[0]+32.0000))));
		algebraic[40] = 1.00000/(algebraic[19]+algebraic[32]);
		rates[9] = (algebraic[6] - states[9])/algebraic[40];
		algebraic[7] = 1.00000/(1.00000+exp((- 8.00000 - states[0])/7.50000));
		algebraic[20] = 1.40000/(1.00000+exp((- 35.0000 - states[0])/13.0000))+0.250000;
		algebraic[33] = 1.40000/(1.00000+exp((states[0]+5.00000)/5.00000));
		algebraic[41] = 1.00000/(1.00000+exp((50.0000 - states[0])/20.0000));
		algebraic[43] =  1.00000*algebraic[20]*algebraic[33]+algebraic[41];
		rates[11] = (algebraic[7] - states[11])/algebraic[43];
		algebraic[59] = (( (( constants[41]*constants[30])/(constants[30]+constants[42]))*states[2])/(states[2]+constants[43]))/(1.00000+ 0.124500*exp(( - 0.100000*states[0]*constants[2])/( constants[0]*constants[1]))+ 0.0353000*exp(( - states[0]*constants[2])/( constants[0]*constants[1])));
		algebraic[44] =  (( constants[0]*constants[1])/constants[2])*log(constants[31]/states[2]);
		algebraic[54] =  constants[36]*pow(states[7], 3.00000)*states[8]*states[9]*(states[0] - algebraic[44]);
		algebraic[55] =  constants[37]*(states[0] - algebraic[44]);
		algebraic[60] = ( constants[44]*( exp(( constants[47]*states[0]*constants[2])/( constants[0]*constants[1]))*pow(states[2], 3.00000)*constants[32] -  exp(( (constants[47] - 1.00000)*states[0]*constants[2])/( constants[0]*constants[1]))*pow(constants[31], 3.00000)*states[3]*constants[46]))/( (pow(constants[49], 3.00000)+pow(constants[31], 3.00000))*(constants[48]+constants[32])*(1.00000+ constants[45]*exp(( (constants[47] - 1.00000)*states[0]*constants[2])/( constants[0]*constants[1]))));
		rates[2] =  (( - 1.00000*(algebraic[54]+algebraic[55]+ 3.00000*algebraic[59]+ 3.00000*algebraic[60]))/( 1.00000*constants[4]*constants[2]))*constants[3];
		algebraic[45] =  (( constants[0]*constants[1])/constants[2])*log(constants[30]/states[1]);
		algebraic[48] = 0.100000/(1.00000+exp( 0.0600000*((states[0] - algebraic[45]) - 200.000)));
		algebraic[49] = ( 3.00000*exp( 0.000200000*((states[0] - algebraic[45])+100.000))+exp( 0.100000*((states[0] - algebraic[45]) - 10.0000)))/(1.00000+exp( - 0.500000*(states[0] - algebraic[45])));
		algebraic[50] = algebraic[48]/(algebraic[48]+algebraic[49]);
		algebraic[51] =  constants[33]*algebraic[50]* pow((constants[30]/5.40000), 1.0 / 2)*(states[0] - algebraic[45]);
		algebraic[58] =  constants[40]*states[16]*states[15]*(states[0] - algebraic[45]);
		algebraic[52] =  constants[34]* pow((constants[30]/5.40000), 1.0 / 2)*states[4]*states[5]*(states[0] - algebraic[45]);
		algebraic[46] =  (( constants[0]*constants[1])/constants[2])*log((constants[30]+ constants[29]*constants[31])/(states[1]+ constants[29]*states[2]));
		algebraic[53] =  constants[35]*pow(states[6], 2.00000)*(states[0] - algebraic[46]);
		algebraic[56] = ( (( constants[38]*states[11]*states[12]*states[13]*states[14]*4.00000*(states[0] - 15.0000)*pow(constants[2], 2.00000))/( constants[0]*constants[1]))*( 0.250000*states[10]*exp(( 2.00000*(states[0] - 15.0000)*constants[2])/( constants[0]*constants[1])) - constants[32]))/(exp(( 2.00000*(states[0] - 15.0000)*constants[2])/( constants[0]*constants[1])) - 1.00000);
		algebraic[47] =  (( 0.500000*constants[0]*constants[1])/constants[2])*log(constants[32]/states[3]);
		algebraic[57] =  constants[39]*(states[0] - algebraic[47]);
		algebraic[62] = ( constants[52]*(states[0] - algebraic[45]))/(1.00000+exp((25.0000 - states[0])/5.98000));
		algebraic[61] = ( constants[50]*states[3])/(states[3]+constants[51]);
		algebraic[13] = (VoI<constants[73] ? 0.00000 : VoI>=constants[73]&&VoI<constants[74] ? constants[73] : VoI>=constants[74]&&VoI<constants[75] ? constants[74] : VoI>=constants[75]&&VoI<constants[76] ? constants[75] : VoI>=constants[76]&&VoI<constants[77] ? constants[76] : VoI>=constants[77]&&VoI<constants[78] ? constants[77] : VoI>=constants[78]&&VoI<constants[79] ? constants[78] : VoI>=constants[79]&&VoI<constants[80] ? constants[79] : VoI>=constants[80]&&VoI<constants[81] ? constants[80] : VoI>=constants[81]&&VoI<constants[82] ? constants[81] : VoI>=constants[82]&&VoI<constants[83] ? constants[82] : VoI+1.00000);
		algebraic[34] = VoI - algebraic[13];
		algebraic[26] = (VoI<constants[73] ? constants[16] : VoI>=constants[73]&&VoI<constants[74] ? constants[17] : VoI>=constants[74]&&VoI<constants[75] ? constants[18] : VoI>=constants[75]&&VoI<constants[76] ? constants[19] : VoI>=constants[76]&&VoI<constants[77] ? constants[20] : VoI>=constants[77]&&VoI<constants[78] ? constants[21] : VoI>=constants[78]&&VoI<constants[79] ? constants[22] : VoI>=constants[79]&&VoI<constants[80] ? constants[23] : VoI>=constants[80]&&VoI<constants[81] ? constants[24] : VoI>=constants[81]&&VoI<constants[82] ? constants[25] : VoI>=constants[82]&&VoI<constants[83] ? constants[26] : constants[16]);
		algebraic[42] = (algebraic[34] -  floor(algebraic[34]/algebraic[26])*algebraic[26]<=constants[28] ? constants[27] : ( - constants[27]*constants[28])/(algebraic[26] - constants[28]));
		rates[0] = - (algebraic[51]+algebraic[58]+algebraic[52]+algebraic[53]+algebraic[56]+algebraic[59]+algebraic[54]+algebraic[55]+algebraic[60]+algebraic[57]+algebraic[62]+algebraic[61]+algebraic[42]);
		rates[1] =  (( - 1.00000*((algebraic[51]+algebraic[58]+algebraic[52]+algebraic[53]+algebraic[62]+algebraic[42]) -  2.00000*algebraic[59]))/( 1.00000*constants[4]*constants[2]))*constants[3];
		algebraic[63] = constants[64]/(1.00000+pow(constants[62], 2.00000)/pow(states[3], 2.00000));
		algebraic[64] =  constants[63]*(states[17] - states[3]);
		algebraic[65] =  constants[61]*(states[10] - states[3]);
		algebraic[67] = 1.00000/(1.00000+( constants[65]*constants[66])/pow(states[3]+constants[66], 2.00000));
		rates[3] =  algebraic[67]*((( (algebraic[64] - algebraic[63])*constants[71])/constants[4]+algebraic[65]) - ( 1.00000*((algebraic[57]+algebraic[61]) -  2.00000*algebraic[60])*constants[3])/( 2.00000*1.00000*constants[4]*constants[2]));
		algebraic[66] = constants[58] - (constants[58] - constants[59])/(1.00000+pow(constants[57]/states[17], 2.00000));
		algebraic[69] =  constants[54]*algebraic[66];
		rates[18] =  - algebraic[69]*states[10]*states[18]+ constants[56]*(1.00000 - states[18]);
		algebraic[68] = constants[53]/algebraic[66];
		algebraic[70] = ( algebraic[68]*pow(states[10], 2.00000)*states[18])/(constants[55]+ algebraic[68]*pow(states[10], 2.00000));
		algebraic[71] =  constants[60]*algebraic[70]*(states[17] - states[10]);
		algebraic[72] = 1.00000/(1.00000+( constants[67]*constants[68])/pow(states[17]+constants[68], 2.00000));
		rates[17] =  algebraic[72]*(algebraic[63] - (algebraic[71]+algebraic[64]));
		algebraic[73] = 1.00000/(1.00000+( constants[69]*constants[70])/pow(states[10]+constants[70], 2.00000));
		rates[10] =  algebraic[73]*((( - 1.00000*algebraic[56]*constants[3])/( 2.00000*1.00000*constants[72]*constants[2])+( algebraic[71]*constants[71])/constants[72]) - ( algebraic[65]*constants[4])/constants[72]);
	}

	void computeAlgebraics(double VoI, double* rates, double* states, double* algebraic){
		algebraic[8] = 1.00000/(1.00000+exp((states[0]+20.0000)/7.00000));
		algebraic[21] =  1102.50*exp(- pow(states[0]+27.0000, 2.00000)/225.000)+200.000/(1.00000+exp((13.0000 - states[0])/10.0000))+180.000/(1.00000+exp((states[0]+30.0000)/10.0000))+20.0000;
		algebraic[9] = 0.670000/(1.00000+exp((states[0]+35.0000)/7.00000))+0.330000;
		algebraic[22] =  562.000*exp(- pow(states[0]+27.0000, 2.00000)/240.000)+31.0000/(1.00000+exp((25.0000 - states[0])/10.0000))+80.0000/(1.00000+exp((states[0]+30.0000)/10.0000));
		algebraic[10] = 0.600000/(1.00000+pow(states[10]/0.0500000, 2.00000))+0.400000;
		algebraic[23] = 80.0000/(1.00000+pow(states[10]/0.0500000, 2.00000))+2.00000;
		algebraic[11] = 1.00000/(1.00000+exp((states[0]+28.0000)/5.00000));
		algebraic[24] =  1000.00*exp(- pow(states[0]+67.0000, 2.00000)/1000.00)+8.00000;
		algebraic[12] = 1.00000/(1.00000+exp((20.0000 - states[0])/6.00000));
		algebraic[25] =  9.50000*exp(- pow(states[0]+40.0000, 2.00000)/1800.00)+0.800000;
		algebraic[1] = 1.00000/(1.00000+exp((- 26.0000 - states[0])/7.00000));
		algebraic[14] = 450.000/(1.00000+exp((- 45.0000 - states[0])/10.0000));
		algebraic[27] = 6.00000/(1.00000+exp((states[0]+30.0000)/11.5000));
		algebraic[35] =  1.00000*algebraic[14]*algebraic[27];
		algebraic[2] = 1.00000/(1.00000+exp((states[0]+88.0000)/24.0000));
		algebraic[15] = 3.00000/(1.00000+exp((- 60.0000 - states[0])/20.0000));
		algebraic[28] = 1.12000/(1.00000+exp((states[0] - 60.0000)/20.0000));
		algebraic[36] =  1.00000*algebraic[15]*algebraic[28];
		algebraic[3] = 1.00000/(1.00000+exp((- 5.00000 - states[0])/14.0000));
		algebraic[16] = 1400.00/ pow((1.00000+exp((5.00000 - states[0])/6.00000)), 1.0 / 2);
		algebraic[29] = 1.00000/(1.00000+exp((states[0] - 35.0000)/15.0000));
		algebraic[37] =  1.00000*algebraic[16]*algebraic[29]+80.0000;
		algebraic[4] = 1.00000/pow(1.00000+exp((- 56.8600 - states[0])/9.03000), 2.00000);
		algebraic[17] = 1.00000/(1.00000+exp((- 60.0000 - states[0])/5.00000));
		algebraic[30] = 0.100000/(1.00000+exp((states[0]+35.0000)/5.00000))+0.100000/(1.00000+exp((states[0] - 50.0000)/200.000));
		algebraic[38] =  1.00000*algebraic[17]*algebraic[30];
		algebraic[5] = 1.00000/pow(1.00000+exp((states[0]+71.5500)/7.43000), 2.00000);
		algebraic[18] = (states[0]<- 40.0000 ?  0.0570000*exp(- (states[0]+80.0000)/6.80000) : 0.00000);
		algebraic[31] = (states[0]<- 40.0000 ?  2.70000*exp( 0.0790000*states[0])+ 310000.*exp( 0.348500*states[0]) : 0.770000/( 0.130000*(1.00000+exp((states[0]+10.6600)/- 11.1000))));
		algebraic[39] = 1.00000/(algebraic[18]+algebraic[31]);
		algebraic[6] = 1.00000/pow(1.00000+exp((states[0]+71.5500)/7.43000), 2.00000);
		algebraic[19] = (states[0]<- 40.0000 ? (( ( - 25428.0*exp( 0.244400*states[0]) -  6.94800e-06*exp( - 0.0439100*states[0]))*(states[0]+37.7800))/1.00000)/(1.00000+exp( 0.311000*(states[0]+79.2300))) : 0.00000);
		algebraic[32] = (states[0]<- 40.0000 ? ( 0.0242400*exp( - 0.0105200*states[0]))/(1.00000+exp( - 0.137800*(states[0]+40.1400))) : ( 0.600000*exp( 0.0570000*states[0]))/(1.00000+exp( - 0.100000*(states[0]+32.0000))));
		algebraic[40] = 1.00000/(algebraic[19]+algebraic[32]);
		algebraic[7] = 1.00000/(1.00000+exp((- 8.00000 - states[0])/7.50000));
		algebraic[20] = 1.40000/(1.00000+exp((- 35.0000 - states[0])/13.0000))+0.250000;
		algebraic[33] = 1.40000/(1.00000+exp((states[0]+5.00000)/5.00000));
		algebraic[41] = 1.00000/(1.00000+exp((50.0000 - states[0])/20.0000));
		algebraic[43] =  1.00000*algebraic[20]*algebraic[33]+algebraic[41];
		algebraic[59] = (( (( constants[41]*constants[30])/(constants[30]+constants[42]))*states[2])/(states[2]+constants[43]))/(1.00000+ 0.124500*exp(( - 0.100000*states[0]*constants[2])/( constants[0]*constants[1]))+ 0.0353000*exp(( - states[0]*constants[2])/( constants[0]*constants[1])));
		algebraic[44] =  (( constants[0]*constants[1])/constants[2])*log(constants[31]/states[2]);
		algebraic[54] =  constants[36]*pow(states[7], 3.00000)*states[8]*states[9]*(states[0] - algebraic[44]);
		algebraic[55] =  constants[37]*(states[0] - algebraic[44]);
		algebraic[60] = ( constants[44]*( exp(( constants[47]*states[0]*constants[2])/( constants[0]*constants[1]))*pow(states[2], 3.00000)*constants[32] -  exp(( (constants[47] - 1.00000)*states[0]*constants[2])/( constants[0]*constants[1]))*pow(constants[31], 3.00000)*states[3]*constants[46]))/( (pow(constants[49], 3.00000)+pow(constants[31], 3.00000))*(constants[48]+constants[32])*(1.00000+ constants[45]*exp(( (constants[47] - 1.00000)*states[0]*constants[2])/( constants[0]*constants[1]))));
		algebraic[45] =  (( constants[0]*constants[1])/constants[2])*log(constants[30]/states[1]);
		algebraic[48] = 0.100000/(1.00000+exp( 0.0600000*((states[0] - algebraic[45]) - 200.000)));
		algebraic[49] = ( 3.00000*exp( 0.000200000*((states[0] - algebraic[45])+100.000))+exp( 0.100000*((states[0] - algebraic[45]) - 10.0000)))/(1.00000+exp( - 0.500000*(states[0] - algebraic[45])));
		algebraic[50] = algebraic[48]/(algebraic[48]+algebraic[49]);
		algebraic[51] =  constants[33]*algebraic[50]* pow((constants[30]/5.40000), 1.0 / 2)*(states[0] - algebraic[45]);
		algebraic[58] =  constants[40]*states[16]*states[15]*(states[0] - algebraic[45]);
		algebraic[52] =  constants[34]* pow((constants[30]/5.40000), 1.0 / 2)*states[4]*states[5]*(states[0] - algebraic[45]);
		algebraic[46] =  (( constants[0]*constants[1])/constants[2])*log((constants[30]+ constants[29]*constants[31])/(states[1]+ constants[29]*states[2]));
		algebraic[53] =  constants[35]*pow(states[6], 2.00000)*(states[0] - algebraic[46]);
		algebraic[56] = ( (( constants[38]*states[11]*states[12]*states[13]*states[14]*4.00000*(states[0] - 15.0000)*pow(constants[2], 2.00000))/( constants[0]*constants[1]))*( 0.250000*states[10]*exp(( 2.00000*(states[0] - 15.0000)*constants[2])/( constants[0]*constants[1])) - constants[32]))/(exp(( 2.00000*(states[0] - 15.0000)*constants[2])/( constants[0]*constants[1])) - 1.00000);
		algebraic[47] =  (( 0.500000*constants[0]*constants[1])/constants[2])*log(constants[32]/states[3]);
		algebraic[57] =  constants[39]*(states[0] - algebraic[47]);
		algebraic[62] = ( constants[52]*(states[0] - algebraic[45]))/(1.00000+exp((25.0000 - states[0])/5.98000));
		algebraic[61] = ( constants[50]*states[3])/(states[3]+constants[51]);
		algebraic[13] = (VoI<constants[73] ? 0.00000 : VoI>=constants[73]&&VoI<constants[74] ? constants[73] : VoI>=constants[74]&&VoI<constants[75] ? constants[74] : VoI>=constants[75]&&VoI<constants[76] ? constants[75] : VoI>=constants[76]&&VoI<constants[77] ? constants[76] : VoI>=constants[77]&&VoI<constants[78] ? constants[77] : VoI>=constants[78]&&VoI<constants[79] ? constants[78] : VoI>=constants[79]&&VoI<constants[80] ? constants[79] : VoI>=constants[80]&&VoI<constants[81] ? constants[80] : VoI>=constants[81]&&VoI<constants[82] ? constants[81] : VoI>=constants[82]&&VoI<constants[83] ? constants[82] : VoI+1.00000);
		algebraic[34] = VoI - algebraic[13];
		algebraic[26] = (VoI<constants[73] ? constants[16] : VoI>=constants[73]&&VoI<constants[74] ? constants[17] : VoI>=constants[74]&&VoI<constants[75] ? constants[18] : VoI>=constants[75]&&VoI<constants[76] ? constants[19] : VoI>=constants[76]&&VoI<constants[77] ? constants[20] : VoI>=constants[77]&&VoI<constants[78] ? constants[21] : VoI>=constants[78]&&VoI<constants[79] ? constants[22] : VoI>=constants[79]&&VoI<constants[80] ? constants[23] : VoI>=constants[80]&&VoI<constants[81] ? constants[24] : VoI>=constants[81]&&VoI<constants[82] ? constants[25] : VoI>=constants[82]&&VoI<constants[83] ? constants[26] : constants[16]);
		algebraic[42] = (algebraic[34] -  floor(algebraic[34]/algebraic[26])*algebraic[26]<=constants[28] ? constants[27] : ( - constants[27]*constants[28])/(algebraic[26] - constants[28]));
		algebraic[63] = constants[64]/(1.00000+pow(constants[62], 2.00000)/pow(states[3], 2.00000));
		algebraic[64] =  constants[63]*(states[17] - states[3]);
		algebraic[65] =  constants[61]*(states[10] - states[3]);
		algebraic[67] = 1.00000/(1.00000+( constants[65]*constants[66])/pow(states[3]+constants[66], 2.00000));
		algebraic[66] = constants[58] - (constants[58] - constants[59])/(1.00000+pow(constants[57]/states[17], 2.00000));
		algebraic[69] =  constants[54]*algebraic[66];
		algebraic[68] = constants[53]/algebraic[66];
		algebraic[70] = ( algebraic[68]*pow(states[10], 2.00000)*states[18])/(constants[55]+ algebraic[68]*pow(states[10], 2.00000));
		algebraic[71] =  constants[60]*algebraic[70]*(states[17] - states[10]);
		algebraic[72] = 1.00000/(1.00000+( constants[67]*constants[68])/pow(states[17]+constants[68], 2.00000));
		algebraic[73] = 1.00000/(1.00000+( constants[69]*constants[70])/pow(states[10]+constants[70], 2.00000));
		algebraic[0] = (VoI<constants[73] ? constants[73] : VoI>=constants[73]&&VoI<constants[74] ? constants[74] : VoI>=constants[74]&&VoI<constants[75] ? constants[75] : VoI>=constants[75]&&VoI<constants[76] ? constants[76] : VoI>=constants[76]&&VoI<constants[77] ? constants[77] : VoI>=constants[77]&&VoI<constants[78] ? constants[78] : VoI>=constants[78]&&VoI<constants[79] ? constants[79] : VoI>=constants[79]&&VoI<constants[80] ? constants[80] : VoI>=constants[80]&&VoI<constants[81] ? constants[81] : VoI>=constants[81]&&VoI<constants[82] ? constants[82] : VoI>=constants[82]&&VoI<constants[83] ? constants[83] : constants[83]);
	}

};
