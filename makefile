CC = c++
LIBS =  -lceledsexporter -lceleds -lmalaes -lcellml

all: main

main: src/cellml2c.cpp

	@echo " "
	@echo "==> Compliling DENIS-CellML2C program to export CellML files to be used in DENIS"
	@echo " "

	$(CC) src/cellml2c.cpp $(LIBS) -o bin/denis-cellml2c
	cp src/C.xml bin/
clean:
	rm bin/C.xml
	rm bin/denis-cellml2c
