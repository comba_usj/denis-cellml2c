/*======================================================================
  
		--- DENIS Project ---
		  -----------------

	 Distributed computing
	 Electrophysiologycal Models
	 Networking colaboration
	 In Silico research
	 Sharing Knowledge
			
 -----------------------------------------------------------------------

                                                 Universidad San Jorge
                                School of Archictecture and Technology

                                                   http://denis.usj.es

 -----------------------------------------------------------------------

   Copyright 2019, J. Carro

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
  implied. See the License for the specific language governing 
  permissions and limitations under the License.

 =====================================================================*/

#include <IfaceCellML_APISPEC.hxx>
#include <IfaceAnnoTools.hxx>
#include <IfaceCeLEDSExporter.hxx>
#include <CeLEDSExporterBootstrap.hpp>
#include <CellMLBootstrap.hpp>
#include <fstream>
#include <string>
#include <cstring>
#include "cellmlFilesList.h"

using namespace std;
using namespace iface::cellml_api;
using namespace iface::cellml_services;

wstring getAbsolutePath(char *exeChar) {
    string executable = exeChar;
    string formatPath = executable.substr(0, executable.find_last_of("/") + 1) + "C.xml";
    wstring wsFormatPath(formatPath.begin(), formatPath.end());

    return wsFormatPath;
}

wstring getFileWstring(const char *modelChar) {
    const size_t cSizeURL = strlen(modelChar) + 1;
    wchar_t *wcURL = new wchar_t[cSizeURL];
    mbstowcs(wcURL, modelChar, cSizeURL);
    wstring wsURL(wcURL);

    return wsURL;
}

wstring getModelUrL(char *modelChar) {
    const size_t cSizeURL = strlen(modelChar) + 1;
    wchar_t *wcURL = new wchar_t[cSizeURL];
    mbstowcs(wcURL, modelChar, cSizeURL);
    wstring wsURL(wcURL);

    return wsURL;
}

wstring getModelPrefix(char *prefixChar) {
    const size_t cSizePrefix = strlen(prefixChar) + 1;
    wchar_t *wcPrefix = new wchar_t[cSizePrefix];
    mbstowcs(wcPrefix, prefixChar, cSizePrefix);
    wstring wsPrefix(wcPrefix);

    return wsPrefix;
}

wstring getCode(wstring wsURL, wstring wsFormatPath) {
    CellMLBootstrap *bootstrap = CreateCellMLBootstrap();
    cout << "Bootstrap created" << endl;
    DOMModelLoader *ml = bootstrap->modelLoader();
    cout << "ModelLoader created" << endl;
    Model *model = ml->loadFromURL(wsURL);
    cout << "Model loaded from URL" << endl;

    CeLEDSExporterBootstrap *celedsexporterb = CreateCeLEDSExporterBootstrap();
    cout << "CeLEDSExproterBootestrap created" << endl;

    CodeExporter *ce = celedsexporterb->createExporter(wsFormatPath);
    cout << "Exporter created" << endl;

    wstring code = ce->generateCode(model);
    cout << "Code generated" << endl;

    return code;
}

char *getFile(char *outputPath, char *prefix) {
    char *file = new char[strlen(outputPath) + strlen(prefix) + 4];
    sprintf(file, "%s/%s.h", outputPath, prefix);

    return file;
}

void printCommentHeaderMainFile(ofstream &ofs) {
    ofs << "/*======================================================================" << endl;
    ofs << "                                                                        " << endl;
    ofs << "                      --- DENIS Project ---                             " << endl;
    ofs << "                        -----------------                               " << endl;
    ofs << "                                                                        " << endl;
    ofs << "      Distributed computing                                             " << endl;
    ofs << "      Electrophysiologycal Models                                       " << endl;
    ofs << "      Networking collaboration                                          " << endl;
    ofs << "      In Silico research                                                " << endl;
    ofs << "      Sharing Knowledge                                                 " << endl;
    ofs << "                                                                        " << endl;
    ofs << "------------------------------------------------------------------------" << endl;
    ofs << "                                                                        " << endl;
    ofs << " -- C Models Library created for the DENIS Project --                   " << endl;
    ofs << "                                                                        " << endl;
    ofs << " This file has been automatically created using the CellML API.         " << endl;
    ofs << "                                                                        " << endl;
    ofs << "------------------------------------------------------------------------" << endl;
    ofs << "                                                                        " << endl;
    ofs << " DENIS-CellML2C Copyright 2020 J. Carro (jcarro@usj.es)                 " << endl;
    ofs << "                                                                        " << endl;
    ofs << " Licensed under the Apache License, Version 2.0 (the \"License\");      " << endl;
    ofs << " you may not use this file except in compliance with the License.       " << endl;
    ofs << " You may obtain a copy of the License at                                " << endl;
    ofs << "                                                                        " << endl;
    ofs << " http://www.apache.org/licenses/LICENSE-2.0                             " << endl;
    ofs << "                                                                        " << endl;
    ofs << " Unless required by applicable law or agreed to in writing, software    " << endl;
    ofs << " distributed under the License is distributed on an \"AS IS\" BASIS,    " << endl;
    ofs << " WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or        " << endl;
    ofs << " implied. See the License for the specific language governing           " << endl;
    ofs << " permissions and limitations under the License.                         " << endl;
    ofs << "                                                                        " << endl;
    ofs << "------------------------------------------------------------------------" << endl;
    ofs << "                                                                        " << endl;
    ofs << "                                                 Universidad San Jorge  " << endl;
    ofs << "                                 School of Architecture and Technology  " << endl;
    ofs << "                                                                        " << endl;
    ofs << "                                                  https://denis.usj.es  " << endl;
    ofs << "                                                                        " << endl;
    ofs << "======================================================================*/" << endl;
    ofs << endl;
}

void printCommentHeaderIndividualFile(wofstream &wofs, wstring prefix) {
    wofs << "/*======================================================================" << endl;
    wofs << "                                                                        " << endl;
    wofs << "                      --- DENIS Project ---                             " << endl;
    wofs << "                        -----------------                               " << endl;
    wofs << "                                                                        " << endl;
    wofs << "      Distributed computing                                             " << endl;
    wofs << "      Electrophysiologycal Models                                       " << endl;
    wofs << "      Networking colaboration                                           " << endl;
    wofs << "      In Silico research                                                " << endl;
    wofs << "      Sharing Knowledge                                                 " << endl;
    wofs << "                                                                        " << endl;
    wofs << "------------------------------------------------------------------------" << endl;
    wofs << "                                                                        " << endl;
    wofs << " -- C File of the " << prefix << " model created for the DENIS Project -- " << endl;
    wofs << "                                                                        " << endl;
    wofs << " This file has been automatically created using the CellML API.         " << endl;
    wofs << "                                                                        " << endl;
    wofs << "------------------------------------------------------------------------" << endl;
    wofs << "                                                                        " << endl;
    wofs << " DENIS-CellML2C Copyright 2020, J. Carro (jcarro@usj.es)                " << endl;
    wofs << "                                                                        " << endl;
    wofs << " Licensed under the Apache License, Version 2.0 (the \"License\");      " << endl;
    wofs << " you may not use this file except in compliance with the License.       " << endl;
    wofs << " You may obtain a copy of the License at                                " << endl;
    wofs << "                                                                        " << endl;
    wofs << " http://www.apache.org/licenses/LICENSE-2.0                             " << endl;
    wofs << "                                                                        " << endl;
    wofs << " Unless required by applicable law or agreed to in writing, software    " << endl;
    wofs << " distributed under the License is distributed on an \"AS IS\" BASIS,    " << endl;
    wofs << " WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or        " << endl;
    wofs << " implied. See the License for the specific language governing           " << endl;
    wofs << " permissions and limitations under the License.                         " << endl;
    wofs << "                                                                        " << endl;
    wofs << "------------------------------------------------------------------------" << endl;
    wofs << "                                                                        " << endl;
    wofs << "                                                 Universidad San Jorge  " << endl;
    wofs << "                                 School of Architecture and Technology  " << endl;
    wofs << "                                                                        " << endl;
    wofs << "                                                  https://denis.usj.es  " << endl;
    wofs << "                                                                        " << endl;
    wofs << "======================================================================*/" << endl;
    wofs << endl;
    wofs << "#include \"Model.h\"" << endl;
    wofs << endl;
    wofs << "#pragma once" << endl;
    wofs << endl;
    wofs << "using namespace std;" << endl;
    wofs << endl;
}

wstring addInitDependentConstsFunction(wstring ws) {

    size_t positionFunction = ws.find(L"void initConstsAndStates(){");
    size_t lineEnd = ws.find(L"\n", positionFunction + 1);
    size_t nextLine = ws.find(L"\n", lineEnd + 1);
    size_t firstConstantInLine = ws.find(L"constants[", lineEnd + 1);
    size_t endFunction = ws.find(L"}", positionFunction + 1);

    bool lineFound = false;

    while (nextLine < endFunction) {
        size_t nextConstant = ws.find(L"constants[", firstConstantInLine + 1);
        if (nextConstant < nextLine) {
            ws.insert(lineEnd + 1, L"\t}\n\n\tvoid setDependentConsts(){\n");
            lineFound = true;
            break;
        }

        lineEnd = nextLine;
        nextLine = ws.find(L"\n", lineEnd + 1);
        firstConstantInLine = ws.find(L"constants[", lineEnd + 1);

    }

    if (!lineFound)
        ws.insert(endFunction + 1, L"\n\n\tvoid setDependentConsts(){}\n");

    return ws;
}

void saveModel(const char *file, wstring code, wstring prefix) {

    wstring newCode = addInitDependentConstsFunction(code);
    cout << file << endl;
    wofstream wofs(file);
    printCommentHeaderIndividualFile(wofs, prefix);
    wofs << "class " << prefix << ":public Model{" << endl;
    wofs << "public:" << endl;
    wofs << "\t" << prefix << "(){" << endl;
    wofs << newCode << endl;
    wofs << "};" << endl;

    wofs.close();
    wcout << "File " << file << " created." << endl;
}

string getCellMLFileName(string s) {
    string out = "";

    const char *cellmlExtension = ".cellml";
    if (s.length() < strlen(cellmlExtension))
        return out;

    int extensionLen = (int) strlen(cellmlExtension);
    string extension = s.substr(s.length() - extensionLen, extensionLen);


    if (strcmp(extension.c_str(), cellmlExtension) == 0)
        out = s.substr(0, s.length() - extensionLen);

    return out;

}

void printIncludes(ofstream &ofs, cellmlFilesList *list) {
    cellmlFilesList *iterator = list;
    ofs << "#pragma once" << endl << endl;
    ofs << "#ifndef MODELS_H" << endl;
    ofs << "#define MODELS_H" << endl << endl;
    ofs << "#include <string.h>" << endl;
    ofs << "#include <math.h>" << endl;

    while (iterator != NULL) {
        ofs << "#include \"models-headers/" << iterator->name << ".h\"" << endl;
        iterator = iterator->next;
    }

    ofs << endl;
}
void printGetModelDefinition(ofstream &ofs, cellmlFilesList *list) {
    cellmlFilesList *iterator = list;

    ofs << "Model* getModelByName(const char* modelName);" << endl << endl;
    ofs << "#endif" << endl;

}

void printGetModel(ofstream &ofs, cellmlFilesList *list) {
    cellmlFilesList *iterator = list;

    ofs << "Model* getModelByName(const char* modelName){" << endl;

    while (iterator != NULL) {
        ofs << "\tif(strcmp(modelName, \"" << iterator->name << "\")==0)" << endl;
        ofs << "\t\treturn new " << iterator->name << "();" << endl;
        iterator = iterator->next;
    }
    ofs << "\treturn NULL;" << endl;
    ofs << "}" << endl;

}

void createModelsSource(cellmlFilesList *list, string path) {
    ofstream ofsh((path + "/models.cpp").c_str());

    printCommentHeaderMainFile(ofsh);
    ofsh << "#include \"models.h\""<< endl <<endl;
    printGetModel(ofsh, list);

    ofsh.close();
}


void createModelsHeader(cellmlFilesList *list, string path) {
    ofstream ofsh((path + "/models.h").c_str());

    printCommentHeaderMainFile(ofsh);
    printIncludes(ofsh, list);
    printGetModelDefinition(ofsh, list);

    ofsh.close();
}
