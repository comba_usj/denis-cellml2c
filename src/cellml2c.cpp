/*==========================================================================
  
		--- DENIS Project ---
		  -----------------

	 Distributed computing
	 Electrophysiologycal Models
	 Networking colaboration
	 In Silico research
	 Sharing Knowledge
			
 ---------------------------------------------------------------------------

                                                   San Jorge University
				                  School of Architecture and Technology
 
                                                   https://denis.usj.es

 ---------------------------------------------------------------------------

   Copyright 2019, J. Carro

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

 ===========================================================================*/

#include "cellml2c.h"
#include <dirent.h>

using namespace iface::cellml_api;
using namespace iface::cellml_services;
using namespace std;


int main(int argc, char **argv) {

    if (argc != 2) {
        cout << "Invalid number of input arguments" << endl << endl;
        cout << "	 cellm2c <models_path>" << endl << endl;
        return EXIT_FAILURE;
    }

    cout << endl << "  === CellML to C conversor for the DENIS Myocyte Simulator ===  " << endl << endl;
    cout << "Models path: " << argv[1] << endl;

    wstring wsAbsolutePath = getAbsolutePath(argv[0]);
    wstring wsModelsFolder = getModelUrL(argv[1]);
    string modelsFolder = string(wsModelsFolder.begin(), wsModelsFolder.end());

    cellmlFilesList *fileList = NULL;
    wstring wsCellMLFilesDirectoryName = wsModelsFolder + L"/models-cellml";
    string cellMLFilesDirectoryName = string(wsCellMLFilesDirectoryName.begin(),
                                             wsCellMLFilesDirectoryName.end());
    wstring wsHeaderFilesDirectoryName = wsModelsFolder + L"/models-headers";
    string headerFilesDirectoryName = string(wsHeaderFilesDirectoryName.begin(),
                                             wsHeaderFilesDirectoryName.end());
    DIR *cellmlFilesDirectory = opendir(cellMLFilesDirectoryName.c_str());

    if (cellmlFilesDirectory != NULL) {
        struct dirent *fileInDirectory;
        while ((fileInDirectory = readdir(cellmlFilesDirectory)) != NULL) {
            string cellMLFileName = getCellMLFileName(fileInDirectory->d_name);

            if (strcmp("", cellMLFileName.c_str()) != 0) {

                cellmlFilesList *newFile = new cellmlFilesList();
                newFile->name = cellMLFileName;
                newFile->next = fileList;
                fileList = newFile;

                printf("%s\n", cellMLFileName.c_str());
                wstring fileCellML = getFileWstring(cellMLFileName.c_str());
                wstring code = getCode(wsCellMLFilesDirectoryName + L"/" +
                                       fileCellML + L".cellml", wsAbsolutePath);
                saveModel((headerFilesDirectoryName + "/" +
                           cellMLFileName + ".h").c_str(), code, fileCellML);
            }
        }
        closedir(cellmlFilesDirectory);
    } else {
        /* could not open directory */
        cout << "Error to open cellmlFilesDirectory " << cellmlFilesDirectory << endl;
        return EXIT_FAILURE;
    }

    int i = 0;
    cellmlFilesList *iterator = fileList;
    while (iterator != NULL) {
        cout << ++i << ") File " << iterator->name << endl;
        iterator = iterator->next;
    }

    createModelsHeader(fileList, modelsFolder);
    createModelsSource(fileList, modelsFolder);
    return 0;
}
